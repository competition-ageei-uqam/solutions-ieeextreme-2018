# Solutions IEEEXtreme 2018
<div>
    <iframe src="https://ctftime.org/event/list/?year=2018&online=1&format=0&restrictions=-1&upcoming=true" frameborder="0"></iframe>
</div>

Ce dépôt contient les solutions développées par les étudiantEs de l'UQAM pour les épreuves de l'IEEEXtreme 12.0.

# Épreuves résolues

 - [Tree Fun](./tree_fun) 30%
 - [Infinite Snakes and Ladders](./snakes_and_ladders) 50%
 - [Barter System](./barter_system) 69%
 - [Drawing Rooted Binary Trees](./drawing_rooted_binary_trees) 0% ~~> 100%
