# Make Distinct

Énoncé: https://csacademy.com/ieeextreme12/task/make-distinct/

You are given an array of *N* integers, *A*. An operation involves taking a number from the array and either adding or subtracting *1* from it. What is the minimum number of operations to make *A* have only distinct elements.

## Standard Input

The first line contains an integer *N*.

The second line contains *N* elements, representing *A*.

## Standard Output

Print the answer on the first line.

## Constraints and notes

- $`1 \le N \le 2 \cdot 10^5`$
- $`1 \le A_i \le N`$ for all valid $`i`$

## Example

|Input|Output|Explanation|
|-----|------|-----------|
|7<br>2 3 4 2 2 1 3<br>[md_1.in](./md_1.in) | `7` | Transform into 2 4 5 -1 1 0 3 with a cost equal to (0 + 1 + 1 + 3 + 1 + 1).|
|4<br>3 3 3 3<br>[md_2.in](./md_2.in) | `4` | Transform into 5 4 3 2.|
|5<br>5 4 4 5 4<br>[md_3.in](./md_3.in) | `4` | Transform into 6 3 4 5 2.|