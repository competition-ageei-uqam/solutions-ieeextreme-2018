from functools import lru_cache
from collections import defaultdict
from math import ceil

# This solution was coded after the ieeextreme12
# Reference Article: http://www.geometer.org/mathcircles/bitmaster.pdf

MAX_PART_SIZE = 14

def correct_bits_for(guess: list) -> int:
    flatten_guess = [item for sublist in guess for item in sublist]
    print('Q', *flatten_guess)
    return int(input())

@lru_cache(maxsize=(2**MAX_PART_SIZE))
def number_of_set_bits(num: int) -> int:
    """Compute Hamming weight and return it."""
    return len([1 for bit in bin(num)[2:] if bit == '1'])

def get_bit_list(num: int, length: int = 0) -> list:
    bit_str = bin(num)[2:]
    if length:
        bit_str = bit_str[:length].zfill(length)
    return list(map(int, bit_str))

@lru_cache(maxsize=2)
def all_permutations_up_to(length: int):
    permutations = defaultdict(list)
    for k in range(length+1):
        permutations[(length, k, 0)] = compute_bit_masks(length, k)
    return permutations

@lru_cache(maxsize=None)
def compute_bit_masks(length: int, set_bits: int) -> list:
    bit_masks = []
    set_bits = min(length, set_bits)
    for num in range(2**length):
        if number_of_set_bits(num) == set_bits:
            bit_masks.append(num)
    return bit_masks

def split(seq: list, size: int) -> list:
    return [seq[pos:pos + size] for pos in range(0, len(seq), size)]

def run_test_case(N: int):
    whole = split([1 for bit in range(N)], MAX_PART_SIZE)
    whole_ok_bits = correct_bits_for(whole)
    for i, part in enumerate(whole):
        whole[i] = [0 for bit in part]
        delta = correct_bits_for(whole) - whole_ok_bits
        whole_ok_bits += delta
        part_1s = (len(part) - delta) // 2
        part_0s = len(part) - part_1s
        part_ok_bits = part_0s

        permutations = all_permutations_up_to(len(part))
        permutations = {key: permutations[key] for key in [(len(part), part_1s, 0)]}
        possibilities[i] = permutations
        last_attempt = 0
        while part_ok_bits < len(part):
            attempt = possibilities[i][(len(part), part_1s, 0)][0]
            whole[i] = get_bit_list(attempt, len(part))
            delta = correct_bits_for(whole) - whole_ok_bits
            whole_ok_bits += delta
            part_ok_bits += delta

            flip_mask = last_attempt ^ attempt
            num_flips = number_of_set_bits(flip_mask)
            bad_flips = (num_flips - delta) // 2
            xor_masks = compute_bit_masks(
                flip_mask.bit_length(), bad_flips)

            unfiltered_comp_masks = [
                (flip_mask ^ xor_mask) & flip_mask for xor_mask in xor_masks]
            
            def comp_filter(m): return number_of_set_bits(m) == num_flips - bad_flips
            comp_masks = list(filter(comp_filter, unfiltered_comp_masks))

            new_possibilities = []
            for num in possibilities[i][(len(part), part_1s, 0)]:
                found = False
                for comp_mask in comp_masks:
                    if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask:
                        found = True
                        break
                if found:
                    new_possibilities.append(num)
            last_attempt = attempt

            possibilities[i][(len(part), part_1s, 0)] = new_possibilities
            if len(new_possibilities) == 1:
                whole[i] = get_bit_list(new_possibilities[0], len(part))
                whole_ok_bits += len(part) - part_ok_bits
                part_ok_bits += len(part) - part_ok_bits
                break

    return [item for sublist in whole for item in sublist]

T = int(input())

for test in range(T):
    N = int(input())

    possibilities = defaultdict()
    for index in range(ceil(N / MAX_PART_SIZE)):
        possibilities[index] = all_permutations_up_to(MAX_PART_SIZE)
    
    print('A', *run_test_case(N))
